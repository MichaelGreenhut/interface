package com.michaelgreenhut.ui;
import flambe.display.ImageSprite;
import flambe.display.Sprite;
import flambe.Entity;
import flambe.input.PointerEvent;
import flambe.util.Signal0;
/*import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.Lib;
import flash.text.TextField;
import flash.text.Font;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
import flash.filters.ColorMatrixFilter;
import openfl.Assets;*/

/**
 * ...
 * @author Michael Greenhut
 */
class Button extends Sprite
{
	private var _up:Sprite;
	private var _over:Sprite;
	private var _down:Sprite;
	private var _current:Sprite;
	private var _toggleButton:Bool = false;
	private var _active:Bool;
	private var _tint:Sprite;
	private var _grey:Bool = false;
	private var _container:Entity;
	private var _pushed:Signal0;
	private var _matrix:Array<Float>;

	public function new(upPath:Sprite, overPath:Sprite, downPath:Sprite, toggleButton:Bool = false) 
	{
		_toggleButton = toggleButton;
		super();
		//buttonMode = true;
		this.owner = new Entity();
		_container = new Entity();
		_up = upPath;
		_down = downPath;
		_over = overPath;
		this.owner.add(this);
		this.owner.addChild(_container);
		switchTo(_up);
	
		this.pointerUp.connect(upState);
		//this.pointerOut.connect(upState);
		//this.pointerIn.connect(overState);
		this.pointerDown.connect(downState);
		_pushed = new Signal0();
//		addEventListener(MouseEvent.MOUSE_UP, overState);
	//	addEventListener(MouseEvent.ROLL_OUT, upState);
	//	addEventListener(MouseEvent.ROLL_OVER, overState);
		//addEventListener(MouseEvent.MOUSE_DOWN, downState);
	}
	
	public function pushed():Signal0
	{
		return _pushed;
	}

	public function addTint(color:Int, alph:Float = 0.33, rounded:Bool = true):Void 
	{
		/*_tint = new Sprite();
		_tint.graphics.beginFill(color,alph);
		
		if (rounded)
			_tint.graphics.drawRoundRect(0,0,width,height,12,12);
		else 
			_tint.graphics.drawRect(0,0,width,height);

		switchTo(_current);
		*/
	}

	public function isGrey()
	{
		return _grey;
	}

	public function grey(value:Bool):Void 
	{
		//_matrix = new Array<Float>();
		_grey = value;
		if (_grey)
		{
			
			//this.transform.colorTransform = new flash.geom.ColorTransform(0.33,0.33,0.33,1);
			
		}
		else 
		{
			//this.transform.colorTransform = new flash.geom.ColorTransform();

			//this.filters = [];

		}

	}
	
	public function toggleOff():Void 
	{
		if (_toggleButton)
		{
			_active = false;
			switchTo(_up);
		}
	}
	
	private function upState(e:PointerEvent):Void 
	{
		if (!_toggleButton)
				switchTo(_up);
	}
	
	private function downState(e:PointerEvent):Void 
	{
		trace("down");
		if (_toggleButton)
		{
			_active = !_active;
			switchTo(_active ? _down : _up);
		}
		else 
			switchTo(_down);
		_pushed.emit();
	}
	
	private function overState(e:PointerEvent):Void 
	{
		if (!_toggleButton)
		{
			switchTo(_over);
		}
			
		
	}
	
	private function switchTo(next:Sprite):Void 
	{
		if (_grey || (_current == next))
			return;
		_current = next;
		_container.add(_current);
		//if (_tint != null)
			//addChild(_tint);
	}
	
	
}
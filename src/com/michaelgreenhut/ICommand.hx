package com.michaelgreenhut;
import flambe.asset.AssetPack;

/**
 * @author Michael Greenhut
 */

interface ICommand 
{
  function execute():Dynamic;
  function setPack(pack:AssetPack):Void;
}
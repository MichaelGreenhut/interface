package urgame;

import com.michaelgreenhut.ICommand;
import flambe.display.Sprite;
import flambe.display.FillSprite;
import flambe.util.Signal0;
import flambe.util.Signal1;
import urgame.commands.AddIcon;
import urgame.commands.AddImageIcon;
import urgame.commands.IMovieCommand;
import urgame.EventHub;
import urgame.Listener;
import urgame.Animator;
import flambe.asset.Manifest;
import flambe.asset.File;
import flambe.asset.AssetPack;
import flambe.Entity;
import flambe.System;
import flambe.swf.MovieSprite;
import flambe.input.PointerEvent;
import flambe.util.SignalConnection;
import flambe.display.Texture;
import flambe.display.ImageSprite;
import urgame.Loader;
import urgame.commands.AddMovieIcon;

/**
 * ...
 * @author Michael Greenhut
 * 
 * TODO:  Implement a careful dispose() function for the animators.  Right now the recursive features make that difficult, so it 
 * will have to wait until this demo becomes a complete project.
 */
class Console extends Listener
{
	
	private var _animator:Animator;
	private var _root:Entity;
	private var _waiting:Bool;
	private var _commandCenter:CommandCenter;
	private var _backgroundEntity:Entity;
	private var _background:Texture;
	private var _fileArray:Array<String>;
	private var _waitSprite:Sprite;
	private var _choiceMap:Map<Sprite,String>;
	private var _doneAnimatingConnection:SignalConnection;
	private var _generalTouchConnection:SignalConnection;
	
	public var backgroundColor:Int = 0xEFEFEE;
	public var animationFinished:Signal0;
	public var signalChoice:Signal1<String>;
	public var signalChoiceTimer:Signal1<String>;
	
	public function new(pack:AssetPack, fileName:String = "animations.xml", createAnimator:Bool = true) 
	{
		super();
		signalChoiceTimer = new Signal1<String>();
		signalChoice = new Signal1<String>();
		_choiceMap = new Map<Sprite,String>();
		_commandCenter = new CommandCenter();
		animationFinished = new Signal0();
		_backgroundEntity = new Entity();
		_root = new Entity();
		EventHub.getInstance().addListener("waitForAction", this, false);
		EventHub.getInstance().addListener("waitForChoice", this, false);
		EventHub.getInstance().addListener("cueAction", this, false);
		EventHub.getInstance().addListener("choiceTimer", this, false);
		_background = System.renderer.createTexture(System.stage.width, System.stage.height);
		_background.graphics.fillRect(backgroundColor,0,0,System.stage.width,System.stage.height);
		_root.addChild(_backgroundEntity.add(new ImageSprite(_background)), false);
		if (createAnimator)
		{
			setAnimator(pack, fileName);
		}
	}
	
	public function switchAnimator(pack:AssetPack, fileName:String):Void 
	{
		
		_doneAnimatingConnection.dispose();
		_root.removeChild(_animator.owner);
		_animator.getAssetPack().dispose();
		_animator.dispose();
		doneAnimating();
		
		setAnimator(pack, fileName);
		_waiting = false;
	}
	
	public override function listen(event:String, sender:Dynamic = null):Void 
	{
		if (event == "waitForAction")
		{
			_waiting = true;
			if (sender != null)
				processWaitCommand(sender);
			 if (_generalTouchConnection == null)
				_generalTouchConnection = System.pointer.down.connect(continueFlow);
		}
		else if (event == "cueAction") 
		{
			
			var actionArgs:Array<String> = sender.action.split(":");
			var command:IMovieCommand = Reflect.callMethod(_commandCenter, Reflect.field(_commandCenter, actionArgs[0].toLowerCase()), [cast(sender.sprite,MovieSprite),actionArgs.length > 1 ? actionArgs[1] : null]);
				command.setAnimator(_animator);
				command.execute();
			
		}
		else if (event == "waitForChoice")
		{
			
			_waiting = true;
			if (_generalTouchConnection != null)
			{
				_generalTouchConnection.dispose();
				_generalTouchConnection = null;
			}
			var ms:Sprite = cast(sender.sprite, Sprite);
			var choiceString:String = cast(sender.choice, String);
			_choiceMap.set(ms, sender.choice);
			trace("waiting for choice", ms);
			ms.pointerDown.connect(function(_) { trace("choice made:", sender.choice);  signalChoice.emit(sender.choice); } ).once();
		}
		else if (event == "choiceTimer")
		{
			//System.pointer.move(continueFlow);
			//var seconds:Float = Std.parseFloat(cast(sender, String));
			signalChoiceTimer.emit(cast(sender,String));
		}
	}
	
	public function continueFlow(pe:PointerEvent = null):Void 
	{
		trace("continuing flow", pe);
		if (pe != null)
		{
			_generalTouchConnection.dispose();
			_generalTouchConnection = null;
		}
		if (_waitSprite != null)
		{
			_root.removeChild(_waitSprite.owner);
			_waitSprite.dispose();
			_waitSprite = null;
		}
		
		_waiting = false;
		EventHub.getInstance().fire("signalContinue");
	}
	
	public function animator():Animator
	{
		return _animator;
	}
	
	private function setAnimator(pack:AssetPack, fileName:String):Void 
	{
		var contents:File = pack.getFile(fileName);
		_animator = new Animator(pack,contents);
		_animator.flumpMovieHandler().setMovieByIndex(0);
		_root.addChild(_animator.owner);
		_doneAnimatingConnection = _animator.animationDone.connect(doneAnimating).once();
	}
	
	private function choice(fileName:String):Void 
	{
		_fileArray = fileName.split("/");
		
        var loader = new Loader([_fileArray[0]]);
		loader.loadingDone().connect(function() { onNewAssetLoaded(loader.getAssets()); } ).once();
	}
	
	private function onNewAssetLoaded(packs:Array<AssetPack>)
	{
		EventHub.getInstance().wipeListeners();
		var contents:File = packs[0].getFile(_fileArray[1]);
		var animator = new Animator(packs[0], contents);
		animator.startingAnimation().connect(function() {
			}).once();
		
		EventHub.getInstance().addListener("waitForAction", this, false);
		EventHub.getInstance().addListener("cueAction", this, false);
		
	}
	
	private function doneAnimating():Void 
	{
		animationFinished.emit();
	}
	
	private function processWaitCommand(actions:Dynamic):Void 
	{
		var args:Array<String> = actions.split(",");
		if (args.length > 1)
		{
			var type:String = args[0].split(":")[1];
			var path:String = args[1].split(":")[1];
		
			if (type == "MovieIcon" || type == "ImageIcon")
			{
				var classString:String = "urgame.commands.Add" + type;
				var IconTypeCommandClass:Class<Dynamic> = (type == "MovieIcon" ? AddMovieIcon : AddImageIcon);
				trace(IconTypeCommandClass, path);
				var itcc:AddIcon = (type == "MovieIcon" ? new AddMovieIcon(args) : new AddImageIcon(args));
				itcc.setPack(_animator.getAssetPack());
				_waitSprite = itcc.execute();
				_root.addChild(new Entity().add(_waitSprite));
			}
		}
		
	}
	
	public function attachAnimator(animator:Animator):Void 
	{
		_animator = animator;
		_animator.flumpMovieHandler().setMovieByIndex(0);
		_root.addChild(_animator.owner);
		_animator.animationDone.connect(doneAnimating).once();
	}
	
	public override function dispose():Void 
	{
		_animator.dispose();
		super.dispose();
	}
	
	public function getRootEntity():Entity
	{
		return _root;
	}
	
}
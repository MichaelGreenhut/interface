package urgame;
import flambe.asset.AssetPack;
import flambe.Component;
import flambe.display.Sprite;
import flambe.Entity;
import flambe.swf.Library;
import flambe.swf.MovieSprite;
import flambe.util.Signal0;
/*

import openfl.Assets;
import flash.display.Sprite;
import flash.events.Event;
import flash.text.Font;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;*/

/**
 * ...
 * @author Michael Greenhut
 */

class TimerView extends Component
{
	//private var _timeField:TextField;
	private var _timerViewRemoved:Signal0;
	private var _count:Int;
	private var _totalSeconds:Int;
	private var _increment:Float;
	private var _view:MovieSprite;
	private var _viewCounter:MovieSprite;
	public static var TIMERVIEW_REMOVED:String = "timerViewRemoved";

	public function new(pack) 
	{
		var library:Library = new Library(pack,"flumpdata/Timer");
		_view = library.createMovie("TimerAnimation");
		this.owner = new Entity();
		this.owner.add(_view);
		_viewCounter = cast(_view.getLayer("Red").firstComponent, MovieSprite);
		
		_viewCounter.paused = true;
		this.owner.add(_view);
		_timerViewRemoved = new Signal0();
		
	}
	
	public function remove():Void 
	{
		
		//if (this.parent != null) 
		{
			this.dispose();
			//this.parent.removeChild(this);
			_timerViewRemoved.emit();
			//dispatchEvent(new Event(TIMERVIEW_REMOVED));
		}
	}
	
	public function timerViewRemoved():Signal0
	{
		return _timerViewRemoved;
	}
	
	public function setSeconds(seconds:Int):Void 
	{
		_totalSeconds = seconds;
	}
	
	public function view():Sprite
	{
		return _view;
	}
	
	public function receiveCount(count:Int):Void 
	{
		_count = count;
		_increment = _viewCounter.symbol.duration / _totalSeconds;
		_viewCounter.position += _increment;
		//_timeField.text = Std.string(_count);
	}
	
}
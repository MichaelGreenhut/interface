package urgame;

import com.michaelgreenhut.ui.Button;
import flambe.Entity;
import flambe.SpeedAdjuster;
import flambe.swf.Library;
import flambe.swf.MovieSprite;
import flambe.System;
import flambe.asset.AssetPack;
import flambe.asset.Manifest;
import flambe.display.FillSprite;
import flambe.display.ImageSprite;
import urgame.Animator;

class Main
{
    private static function main ()
    {
        // Wind up all platform-specific stuff
        System.init();

        // Load up the compiled pack in the assets directory named "bootstrap"
        var manifest = Manifest.fromAssetsLocalized("bootstrap");
        var loader = System.loadAssetPack(manifest);
        loader.get(onSuccess);
    }

    private static function onSuccess (pack :AssetPack)
    {
		trace("aah");
		//trace("***" + System.pointer.move);
		/*
		var timer:ChoiceTimer = new ChoiceTimer(7);
		var plane:ImageSprite = new ImageSprite(pack.getTexture("plane"));
		var panel1:ImageSprite = new ImageSprite(pack.getTexture("plane"));
		panel1.alpha._ = 0.5;
		var panel2:ImageSprite = new ImageSprite(pack.getTexture("plane"));
		panel2.alpha._ = 0.25;
		var button:Button = new Button(plane, panel1, panel2);
		System.root.addChild(button.owner);
		var timerView:TimerView = new TimerView(pack);
		timer.addView(timerView);
		
		System.root.addChild(timerView.owner);
		timer.timerDone().connect(function() { trace (100 / 0); } );
		timer.start();*/
		
		
		var console:Console = new Console([pack]);
		System.root.addChild(console.getRootEntity());
        // Add a solid color background
        //var background = new FillSprite(0x202020, System.stage.width, System.stage.height);
        //System.root.addChild(new Entity().add(background));

        // Add a plane that moves along the screen
        //var plane = new ImageSprite(pack.getTexture("plane"));
        //plane.x._ = 30;
        //plane.y.animateTo(200, 6);
        //System.root.addChild(new Entity().add(plane));
    }
	
}

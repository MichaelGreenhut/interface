package urgame.commands;
import com.michaelgreenhut.ICommand;
import flambe.asset.AssetPack;

/**
 * ...
 * @author Michael Greenhut
 */
class AddIcon implements ICommand
{
	private var _pack:AssetPack;
	private var _receiver:Array<String>;

	public function new(receiver:Array<String>) 
	{
		
		_receiver = receiver;
	}
	
	public function setPack(pack:AssetPack):Void 
	{
		_pack = pack;
	}
	
	public function execute():Dynamic
	{
		//for override only
		return null;
	}
	
	public function dispose():Void 
	{
		_receiver = null;
		_pack = null;
	}
	
}
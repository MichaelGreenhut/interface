package urgame.commands;
import flambe.swf.MovieSprite;
import urgame.Animator;

/**
 * ...
 * @author Michael Greenhut
 */
class StopMovie implements IMovieCommand
{
	private var _receiver:MovieSprite;
	private var _animator:Animator;
	private var _subMovie:String;

	public function new(receiver:MovieSprite, subMovie:String = null) 
	{
		_subMovie = subMovie;
		_receiver = receiver;
	}
	
	/* INTERFACE urgame.commands.IMovieCommand */
	
	public function execute():Void 
	{
		_animator.flumpMovieHandler().setMovieByInstance(_receiver);
		if (_subMovie != null)
			_animator.flumpMovieHandler().stopInnerMovie(_receiver, _subMovie);
		else 
			_animator.flumpMovieHandler().stopCurrentMovie();
	}
	
	public function setAnimator(animator:Animator):Void 
	{
		_animator = animator;
	}
	
	public function dispose():Void 
	{
		_animator = null;
		_subMovie = null;
		_receiver = null;
	}
	
}
package urgame.commands;
import urgame.Animator;

/**
 * @author Michael Greenhut
 */

interface IMovieCommand 
{
   function execute():Void;
   function setAnimator(animator:Animator):Void;
}
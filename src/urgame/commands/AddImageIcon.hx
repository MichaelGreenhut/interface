package urgame.commands;
import com.michaelgreenhut.ICommand;
import flambe.asset.AssetPack;
import flambe.display.ImageSprite;

/**
 * ...
 * @author Michael Greenhut
 */
class AddImageIcon extends AddIcon implements ICommand
{

	public function new(receiver:Array<String>) 
	{
		super(receiver);
		//_receiver = receiver;
	}
	
	public override function execute():Dynamic
	{
		//var args:Array<String> = _receiver.split(",");
		var path:String = _receiver[1].split(":")[1];
		var xPos:Float = Std.parseFloat(_receiver[2].split(":")[1]);
		var yPos:Float = Std.parseFloat(_receiver[3].split(":")[1]);
		
		var sprite:ImageSprite = new ImageSprite(_pack.getTexture(path));
		sprite.x._ = xPos;
		sprite.y._ = yPos;
		
		return sprite;
	}
	
}
package urgame.commands;
import flambe.swf.MovieSprite;
import urgame.Animator;

/**
 * ...
 * @author Michael Greenhut
 */

 
class PlayMovie implements IMovieCommand
{
	private var _receiver:MovieSprite;
	private var _animator:Animator;
	private var _subMovie:String;

	public function new(receiver:MovieSprite, subMovie:String = null) 
	{
		_subMovie = subMovie;
		_receiver = receiver;
	}
	
	/* INTERFACE urgame.commands.IMovieCommand */
	
	public function execute():Void 
	{
		_animator.flumpMovieHandler().setMovieByInstance(_receiver);
		if (_subMovie != null)
			_animator.flumpMovieHandler().toggleLoop(false, _animator.flumpMovieHandler().layer(_subMovie));
		else 
			_animator.flumpMovieHandler().toggleOuterMovie(_receiver, true, false);
		
	}
	
	public function setAnimator(animator:Animator):Void 
	{
		_animator = animator;
	}
	
	public function dispose():Void 
	{
		_animator = null;
		_subMovie = null;
		_receiver = null;
	}
	

	
}
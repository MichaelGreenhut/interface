package urgame.commands;
import com.michaelgreenhut.ICommand;
import flambe.asset.AssetPack;
import flambe.Entity;
import flambe.swf.Library;
import flambe.swf.MovieSprite;

/**
 * ...
 * @author Michael Greenhut
 */
class AddMovieIcon extends AddIcon implements ICommand
{

	public function new(receiver:Array<String>) 
	{
		super(receiver);
	}
	
	public override function execute():Dynamic
	{
		var path:String = _receiver[1].split(":")[1];
		var xPos:Float = Std.parseFloat(_receiver[2].split(":")[1]);
		var yPos:Float = Std.parseFloat(_receiver[3].split(":")[1]);
		
		var location:Array<String> = path.split("/");
		var locationBody:Array<String> = location.concat([]);
		locationBody.pop();
		var library:Library = new Library(_pack, locationBody.join("/"));
		var movie:MovieSprite = library.createMovie(location[location.length - 1]);
		movie.x._ = xPos;
		movie.y._ = yPos;
		return movie;
	}
	
	
	
}
package urgame.commands;
import flambe.swf.MovieSprite;

/**
 * ...
 * @author Michael Greenhut
 */
class CycleMovie extends PlayMovie
{

	public function new(receiver:MovieSprite, subMovie:String = null) 
	{
		super(receiver, subMovie);
	}
	
	public override function execute():Void 
	{
		_animator.flumpMovieHandler().setMovieByInstance(_receiver);
		
		if (_subMovie != null)
			_animator.flumpMovieHandler().toggleInnerMovies(_receiver, true, _subMovie, true);
		else
			_animator.flumpMovieHandler().toggleOuterMovie(_receiver, true, true);
			
	}
	
}
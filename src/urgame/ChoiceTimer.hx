package urgame;
import flambe.Component;
import flambe.display.Sprite;
import flambe.Entity;
import flambe.util.Signal0;
import haxe.Timer;
//import com.michaelgreenhut.CustomEvent;
//import flash.display.Sprite;
//import flash.events.Event;
//import flash.Lib;

/**
 * ...
 * @author Michael Greenhut
 */

class ChoiceTimer extends Component
{
	private var _counter:Int = 0;
	private var _frames:Int = 0;
	private var _totalSeconds:Int;
	private var _timerView:TimerView;
	private var _timerDone:Signal0;
	private var _timer:Timer;
	public static var TICK:String = "tick";
	//public static var TIMER_DONE:String = "timerDone";

	public function new(totalSeconds:Int = 10) 
	{
		_totalSeconds = totalSeconds;
		_timerDone = new Signal0();
		
	}
	
	public function addView(view:TimerView):Void 
	{
		
		_timerView = view;
		

		//addChild(_timerView);
		view.setSeconds(_totalSeconds);
	}
	
	public function start():Void 
	{
		//trace("start");
		_counter = _totalSeconds;
		_timer = new Timer(1000);
		_timer.run = count;
		//addEventListener(Event.ENTER_FRAME, count);
	}
	
	public function stop():Void 
	{
		_timer.stop();
		//removeEventListener(Event.ENTER_FRAME, count);
	}
	
	public function getCount():Int 
	{
		return _counter;
	}
	
	private function count():Void 
	{
		trace("count", _counter);
		//_frames++;
		//if ((_frames % Lib.current.stage.frameRate) == 0)
		//{
			_counter--;
			if (_timerView != null)
				_timerView.receiveCount(_counter);
			
		//}
		
		if (_counter == 0)
		{
			//removeEventListener(Event.ENTER_FRAME, count);
			_timer.stop();
			_timerDone.emit();
			if (_timerView != null)
			{
				//_timerView.timerViewRemoved().connect(remove);
				//_timerView.addEventListener(TimerView.TIMERVIEW_REMOVED, remove);
				_timerView.remove();
				remove();
			}
		}
		
	}
	
	public function reset():Void 
	{
		_counter = _totalSeconds;
	}
	
	public function timerDone():Signal0
	{
		return _timerDone;
	}
	
	private function remove():Void 
	{
		dispose();
		/*if (this.parent != null) 
		{
			this.parent.removeChild(this);
		}*/
	}
	
}
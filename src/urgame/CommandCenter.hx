package urgame;
import com.michaelgreenhut.ICommand;
import flambe.swf.MovieSprite;
import urgame.commands.CycleMovie;
import urgame.commands.IMovieCommand;
import urgame.commands.PlayMovie;
import urgame.commands.StopMovie;

/**
 * ...
 * @author Michael Greenhut
 */

 
class CommandCenter
{
	private var _playMovie:PlayMovie;
	private var _cycleMovie:CycleMovie;
	private var _stopMovie:StopMovie;

	public function new() 
	{
		//_playMovie = new PlayMovie();
		play(null);
		cycle(null);
		stop(null);
	}
	
	public function getCommand(command:String, movie:MovieSprite):Void 
	{
		
	}
	
	public function play(movie:MovieSprite, subMovie:String = null):IMovieCommand
	{
		//trace("play test tested");
		if (_playMovie == null && movie != null)
			_playMovie = new PlayMovie(movie,subMovie);
		return _playMovie;
	}
	
	public function cycle(movie:MovieSprite, subMovie:String = null):IMovieCommand
	{
		if (_cycleMovie == null && movie != null)
			_cycleMovie = new CycleMovie(movie, subMovie);
		return _cycleMovie;
	}
	
	public function stop(movie:MovieSprite, subMovie:String = null):IMovieCommand
	{
		if (_stopMovie == null && movie != null)
			_stopMovie = new StopMovie(movie, subMovie);
		return _stopMovie;
	}
	
	/*public function cycle():Label 
	{
		return {
			command: 
		}
	}*/
	
}
package urgame;
import flambe.asset.AssetEntry;
import flambe.asset.AssetPack;
import flambe.asset.File;
import flambe.asset.Manifest;
import flambe.display.Font;
import flambe.display.Sprite;
import flambe.display.TextSprite;
import flambe.Entity;
import flambe.System;
import flambe.util.SignalConnection;
import haxe.xml.Fast;

/**
 * ...
 * @author Michael Greenhut
 */
class StoryBuilder
{
	private var _console:Console;
	private var _packNames:Array<String>;
	private var _storyMap:Map<String,String>;
	private var _choiceTimerConnection:SignalConnection;
	private var _choiceConnection:SignalConnection;
	private var _container:Sprite;
	private var _fileName:String;
	private var _mapFileName:String;
	private var _timer:ChoiceTimer;
	private var _currentPack:AssetPack;
	private var _timerView:TimerView;
	private var _choiceText:TextSprite;

	public function new(packnames:Array<String>, mapfilename:String = "storymap.xml") 
	{
		_mapFileName = mapfilename;
		_storyMap = new Map<String,String>();
		
		_packNames = packnames;
		_fileName = "animations.xml";
		
		nextPack(_packNames[0]);
	}
	
	private function nextPack(packname:String) 
	{	
		var manifest = Manifest.fromAssets(packname);
		var loader = System.loadAssetPack(manifest);
		loader.get(onPackLoaded);
    }
	
	 private function onPackLoaded (pack :AssetPack)
    {
		_currentPack = pack;
		
		var it:Iterator<AssetEntry> = _currentPack.manifest.iterator();
		for (it in _currentPack.manifest)
		{
			if (it.name == _mapFileName)
			{
				var contents:File = pack.getFile(_mapFileName);
				begin(contents.toString());
				break;	
			}
			
		}
        
		if (_console == null)
		{
			_console = new Console(pack, _fileName);
			_choiceTimerConnection = _console.signalChoiceTimer.connect(handleChoiceTimer);
			_choiceConnection = _console.signalChoice.connect(handleChoice);
			_container = new Sprite();
			new Entity().add(_container);
		}
		else
			_console.switchAnimator(_currentPack, _fileName);
		_container.owner.addChild(_console.getRootEntity());
		
		//_console.animationFinished.connect(removeConsole).once();
		System.root.addChild(_container.owner);
		
    }
	
	private function begin(xmldata:String):Void 
	{
		var xmlString = Xml.parse(xmldata);
		var fast:Fast = new Fast(xmlString);
		//_images = new Array<Dynamic>();
		for (choice in fast.nodes.choice)
		{
			_storyMap.set(choice.att.name, choice.att.assetPack + ":" + choice.att.file);
		}

	}
	
	private function handleChoiceTimer(choiceTimer:String):Void 
	{
		var choiceArray:Array<String> = choiceTimer.split(":");
		var time:Float = Std.parseFloat(choiceArray[0]);
		_choiceText = new TextSprite(new Font(_currentPack, "GEORGIA"), choiceArray[1]);
		new Entity().add(_choiceText);
		_timer = new ChoiceTimer(Std.int(time));
		_timerView = new TimerView(_currentPack);
		_timer.addView(_timerView);
		_timerView.view().x._ = System.stage.width / 2 - Sprite.getBounds(_timerView.owner).width / 2;
		_timerView.view().y._ = System.stage.height / 2 - Sprite.getBounds(_timerView.owner).height / 2;
		_choiceText.x._ = _timerView.view().x._ + Sprite.getBounds(_timerView.owner).width / 2 - Sprite.getBounds(_choiceText.owner).width / 2;
		_choiceText.y._ = _timerView.view().y._ + Sprite.getBounds(_timerView.owner).height + 10;
		
		_container.owner.addChild(_timerView.owner);
		_container.owner.addChild(_choiceText.owner);
		_timer.timerDone().connect(timeOver).once();
		_timer.start();
		//count down timer.  If no choice is made, continue along same animator file. 
	}
	
	private function timeOver():Void 
	{
		removeTimer();
		_console.continueFlow();
		//_timerView.remove();
	}
	
	private function handleChoice(choice:String):Void 
	{
		_timer.stop();
		removeTimer();
		trace("choice:", _storyMap.get(choice));
		_fileName = _storyMap.get(choice).split(":")[1];
		//_console.getRootEntity().firstComp
		nextPack(_storyMap.get(choice).split(":")[0]);
		//map choice to files/asset pack in the choicemap, load asset pack and file, attach to console. 
	}
	
	private function removeTimer():Void 
	{
		_container.owner.removeChild(_timerView.owner);
		_container.owner.removeChild(_choiceText.owner);
	}
	
}